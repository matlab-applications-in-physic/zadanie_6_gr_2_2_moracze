//Matlab applications in physics
//Author: Izabela Moraczewska
//Technical Physics

clear;

while True


//reading data in JSON
katURL=JSONParse(mgetl(getURL ("https://danepubliczne.imgw.pl/api/data/synop/id/12560")));
czesURL=JSONParse(mgetl(getURL ("https://danepubliczne.imgw.pl/api/data/synop/id/12550")));
raURL=JSONParse(mgetl(getURL ("https://danepubliczne.imgw.pl/api/data/synop/id/12540")));
//creating matrixes for data  
kDane = ['temperaura', 'cisnienie', 'predkość_wiatru','kierunek_wiatru', 'wilgotnosc_wzgledna';'katoTemperatura','katoCisnienie','katoPredkosc_wiatru','katoKierunek_wiatru','katoWilgotnosc_wzgledna'];
czDane = ['temperaura', 'cisnienie', 'predkość_wiatru','kierunek_wiatru', 'wilgotnosc_wzgledna';'czTemperatura','czCisnienie','czPredkosc_wiatru','czKierunek_wiatru','czWilgotnosc_wzgledna'];
raDane = ['temperaura', 'cisnienie', 'predkość_wiatru','kierunek_wiatru', 'wilgotnosc_wzgledna';'raTemperatura','raCisnienie','raPredkosc_wiatru','raKierunek_wiatru','raWilgotnosc_wzgledna'];
//adding data to matrixes
kDane(2,1) = katURL.temperatura;
kDane(2,2) = katURL.cisnienie;
kDane(2,3) = katURL.predkosc_wiatru;
kDane(2,4) = katURL.kierunek_wiatru;
kDane(2,5) = katURL.wilgotnosc_wzgledna;
kDane(2,6) = 13.12 + 0.6215*kDane(2,1) - 11.37*kDane(2,3)**0.16 + 0.3965*kDane(2,1)*kDane(2,3)**0.16

czDane(2,1) = czesURL.temperatura;
czDane(2,2) = czesURL.cisnienie;
czDane(2,3) = czesURL.predkosc_wiatru;
czDane(2,4) = czesURL.kierunek_wiatru;
czDane(2,5) = czesURL.wilgotnosc_wzgledna;
czDane(2,6) = 13.12 + 0.6215*czDane(2,1) - 11.37*czDane(2,3)**0.16 + 0.3965*czDane(2,1)*czDane(2,3)**0.16

raDane(2,1) = raURL.temperatura;
raDane(2,2) = raURL.cisnienie;
raDane(2,3) = raURL.predkosc_wiatru;
raDane(2,4) = raURL.kierunek_wiatru;
raDane(2,5) = raURL.wilgotnosc_wzgledna;
raDane(2,6) = 13.12 + 0.6215*raDane(2,1) - 11.37*raDane(2,3)**0.16 + 0.3965*raDane(2,1)*raDane(2,3)**0.16



//following code is using different source of data 
//reading date in JSON 
rachURL=JSONParse(mgetl(getURL ("http://wttr.in/raciborz?format=j1")));
katchURL=JSONParse(mgetl(getURL ("https://wttr.in/katowice?format=j1")));
czchURL=JSONParse(mgetl(getURL ("https://wttr.in/czestochowa?format=j1")));
//creating matrixes for data
kchDane = ['temperaura', 'cisnienie', 'predkość_wiatru','kierunek_wiatru', 'wilgotnosc_wzgledna';'katoTemperatura','katoCisnienie','katoPredkosc_wiatru','katoKierunek_wiatru','katoWilgotnosc_wzgledna'];
rachDane = ['temperaura', 'cisnienie', 'predkość_wiatru','kierunek_wiatru', 'wilgotnosc_wzgledna';'katoTemperatura','katoCisnienie','katoPredkosc_wiatru','katoKierunek_wiatru','katoWilgotnosc_wzgledna'];
kczeschDane = ['temperaura', 'cisnienie', 'predkość_wiatru','kierunek_wiatru', 'wilgotnosc_wzgledna';'katoTemperatura','katoCisnienie','katoPredkosc_wiatru','katoKierunek_wiatru','katoWilgotnosc_wzgledna'];
//adding data to matrixes
kchDane(2,1) = katchURL.current_condition.temp_C;
kchDane(2,2) = katchURL.current_condition.pressure;
kchDane(2,3) = katchURL.current_condition.windspeedKmph;
kchDane(2,4) = katchURL.current_condition.winddirDegree;
kchDane(2,5) = katchURL.current_condition.humidity;
katchURL.current_condition.observation_time
rachDane(2,1) = katchURL.current_condition.temp_C;
rachDane(2,2) = katchURL.current_condition.pressure;
rachDane(2,3) = katchURL.current_condition.windspeedKmph;
rachDane(2,4) = katchURL.current_condition.winddirDegree;
rachDane(2,5) = katchURL.current_condition.humidity;
rachURL.current_condition.observation_time
kczeschDane(2,1) = katchURL.current_condition.temp_C;
kczeschDane(2,2) = katchURL.current_condition.pressure;
kczeschDane(2,3) = katchURL.current_condition.windspeedKmph;
kczeschDane(2,4) = katchURL.current_condition.winddirDegree;
kczeschDane(2,5) = katchURL.current_condition.humidity;
czchURL.current_condition.observation_time

//creating files with matrixes of data
Pogodak = fullfile(TMPDIR, "weather_m.csv");
csvWrite(kDane, Pogodak);
 
Pogodacz = fullfile(TMPDIR, "weather_m1.csv");
csvWrite(czDane, Pogodacz);

Pogodara = fullfile(TMPDIR, "weather_m2.csv");
csvWrite(raDane, Pogodara);

Pogodaka = fullfile(TMPDIR, "weather_m3.csv");
csvWrite(kchDane, Pogodak);
 
Pogodacze = fullfile(TMPDIR, "weather_m4.csv");
csvWrite(rachDane, Pogodacz);

Pogodarach = fullfile(TMPDIR, "weather_m5.csv");
csvWrite(kczeschDane, Pogodara);//Matlab applications in physics
//Author: Izabela Moraczewska
//Technical Physics

clear;
//reading data in JSON
katURL=JSONParse(mgetl(getURL ("https://danepubliczne.imgw.pl/api/data/synop/id/12560")));
czesURL=JSONParse(mgetl(getURL ("https://danepubliczne.imgw.pl/api/data/synop/id/12550")));
raURL=JSONParse(mgetl(getURL ("https://danepubliczne.imgw.pl/api/data/synop/id/12540")));
//creating matrixes for data  
kDane = ['temperaura', 'cisnienie', 'predkość_wiatru','kierunek_wiatru', 'wilgotnosc_wzgledna';'katoTemperatura','katoCisnienie','katoPredkosc_wiatru','katoKierunek_wiatru','katoWilgotnosc_wzgledna'];
czDane = ['temperaura', 'cisnienie', 'predkość_wiatru','kierunek_wiatru', 'wilgotnosc_wzgledna';'czTemperatura','czCisnienie','czPredkosc_wiatru','czKierunek_wiatru','czWilgotnosc_wzgledna'];
raDane = ['temperaura', 'cisnienie', 'predkość_wiatru','kierunek_wiatru', 'wilgotnosc_wzgledna';'raTemperatura','raCisnienie','raPredkosc_wiatru','raKierunek_wiatru','raWilgotnosc_wzgledna'];
//adding data to matrixes
kDane(2,1) = katURL.temperatura;
kDane(2,2) = katURL.cisnienie;
kDane(2,3) = katURL.predkosc_wiatru;
kDane(2,4) = katURL.kierunek_wiatru;
kDane(2,5) = katURL.wilgotnosc_wzgledna;

czDane(2,1) = czesURL.temperatura;
czDane(2,2) = czesURL.cisnienie;
czDane(2,3) = czesURL.predkosc_wiatru;
czDane(2,4) = czesURL.kierunek_wiatru;
czDane(2,5) = czesURL.wilgotnosc_wzgledna;

raDane(2,1) = raURL.temperatura;
raDane(2,2) = raURL.cisnienie;
raDane(2,3) = raURL.predkosc_wiatru;
raDane(2,4) = raURL.kierunek_wiatru;
raDane(2,5) = raURL.wilgotnosc_wzgledna;

//TflK = 13.12 + 0.6215*kDane(2,1) - 11.37*kDane(2,3)**0.16 + 0.3965*kDane(2,1)*kDane(2,3)**0.16

//following code is using different source of data 
//reading date in JSON 
rachURL=JSONParse(mgetl(getURL ("http://wttr.in/raciborz?format=j1")));
katchURL=JSONParse(mgetl(getURL ("https://wttr.in/katowice?format=j1")));
czchURL=JSONParse(mgetl(getURL ("https://wttr.in/czestochowa?format=j1")));
//creating matrixes for data
kchDane = ['temperaura', 'cisnienie', 'predkość_wiatru','kierunek_wiatru', 'wilgotnosc_wzgledna';'katoTemperatura','katoCisnienie','katoPredkosc_wiatru','katoKierunek_wiatru','katoWilgotnosc_wzgledna'];
rachDane = ['temperaura', 'cisnienie', 'predkość_wiatru','kierunek_wiatru', 'wilgotnosc_wzgledna';'katoTemperatura','katoCisnienie','katoPredkosc_wiatru','katoKierunek_wiatru','katoWilgotnosc_wzgledna'];
kczeschDane = ['temperaura', 'cisnienie', 'predkość_wiatru','kierunek_wiatru', 'wilgotnosc_wzgledna';'katoTemperatura','katoCisnienie','katoPredkosc_wiatru','katoKierunek_wiatru','katoWilgotnosc_wzgledna'];
//adding data to matrixes
kchDane(2,1) = katchURL.current_condition.temp_C;
kchDane(2,2) = katchURL.current_condition.pressure;
kchDane(2,3) = katchURL.current_condition.windspeedKmph;
kchDane(2,4) = katchURL.current_condition.winddirDegree;
kchDane(2,5) = katchURL.current_condition.humidity;
kchDane(2,6) = katchURL.current_condition.FeelsLikeC
katchURL.current_condition.observation_time
rachDane(2,1) = katchURL.current_condition.temp_C;
rachDane(2,2) = katchURL.current_condition.pressure;
rachDane(2,3) = katchURL.current_condition.windspeedKmph;
rachDane(2,4) = katchURL.current_condition.winddirDegree;
rachDane(2,5) = katchURL.current_condition.humidity;
rachDane(2,6) = katchURL.current_condition.FeelsLikeC
rachURL.current_condition.observation_time
kczeschDane(2,1) = katchURL.current_condition.temp_C;
kczeschDane(2,2) = katchURL.current_condition.pressure;
kczeschDane(2,3) = katchURL.current_condition.windspeedKmph;
kczeschDane(2,4) = katchURL.current_condition.winddirDegree;
kczeschDane(2,5) = katchURL.current_condition.humidity;
kczeschDane(2,6) = katchURL.current_condition.FeelsLikeC
czchURL.current_condition.observation_time

//creating files with matrixes of data
Pogodak = fullfile(TMPDIR, "weather_m.csv");
csvWrite(kDane, Pogodak);
 
Pogodacz = fullfile(TMPDIR, "weather_m1.csv");
csvWrite(czDane, Pogodacz);

Pogodara = fullfile(TMPDIR, "weather_m2.csv");
csvWrite(raDane, Pogodara);

Pogodaka = fullfile(TMPDIR, "weather_m3.csv");
csvWrite(kchDane, Pogodak);
 
Pogodacze = fullfile(TMPDIR, "weather_m4.csv");
csvWrite(rachDane, Pogodacz);

Pogodarach = fullfile(TMPDIR, "weather_m5.csv");
csvWrite(kczeschDane, Pogodara);



end
